import cx_Oracle
import pandas as pd
#connect to DB - note that bci-internal details are saved locally in tnsnames.ora
connection= cx_Oracle.connect("admin", "Bc12019sds", "bci-internal")

#Define a parameter to access the cursor method. This parameter connects to the Oracle instance:

cursor=connection.cursor()

#Create a query string:
querystring="select *from PROJECTS.ROLES"
df=pd.read_sql(querystring,connection)
#pass the query string to the cursor method

print(df.to_string())