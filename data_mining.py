import PyPDF2
import data_func
import json

def split_tableofcontents(path):
    reader = PyPDF2.PdfFileReader(path)

    print(reader.documentInfo)

    num_of_pages = reader.numPages
    print('Number of pages: ' + str(num_of_pages))

    writer = PyPDF2.PdfFileWriter()

    for page in range(4, 5):
        writer.addPage(reader.getPage(page))

    output_filename = './data/original/table_of_contents.pdf'

    with open(output_filename, 'wb') as output:
        writer.write(output)


    text = data_func.convert_pdf_to_string(
        './data/original/table_of_contents.pdf')
    #print(text[:1000])

    text = text.replace('.','')
    text = text.replace('\x0c','')
    table_of_contents_raw = text.split('\n')
    print(table_of_contents_raw)

    title_list = []
    pagenum_list = []
    title_formatted_list = []
    for item in table_of_contents_raw:
        title, pagenum = \
            data_func.split_to_title_and_pagenum(item)
        if title != None:
            title_list.append(title)
            pagenum_list.append(pagenum)
            title_formatted_list.append(
                data_func.convert_title_to_filename(title))

    # for page_list, we need to add the last page as well
    pagenum_list.append(num_of_pages + 1-21)
    print(pagenum_list)
    print(title_list)
    for i in range(1, len(title_formatted_list)):
        title_formatted = title_formatted_list[i]
        page_start = pagenum_list[i] - 1 + 21
        page_end = pagenum_list[i+1] - 2 + 21

        writer = PyPDF2.PdfFileWriter()

        for page in range(page_start, page_end + 1):
            writer.addPage(reader.getPage(page))

        output_filename = './data/pdfs/' +  title_formatted + '.pdf'

        with open(output_filename, 'wb') as output:
            writer.write(output)

    #  saving tale of contents in json


    with open('./data/original/table_of_contents.json', 'a') as f:
        for title, pagenum in zip(title_list, pagenum_list):
            f.write(json.dumps({'title': title, 'pagenum': pagenum}))

split_tableofcontents('./data/original/tenderspec.pdf')