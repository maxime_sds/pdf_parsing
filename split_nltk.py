from io import StringIO

from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfdocument import PDFDocument
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage
from pdfminer.pdfparser import PDFParser
from nltk.tokenize import sent_tokenize
import data_func

def convert_pdf_to_txt(path):
    text = data_func.convert_pdf_to_string(path)
    sentences = sent_tokenize(text)

    string_to_search="PA "
    output = open("out09.txt", 'w', encoding="utf-8")
    for s in sentences:
        #s = s.replace("-\n", '') #remove hyphens
        lines = s.split("\n")
        for line in lines:
            if string_to_search in line:
                line = "--SECTION-- " +line
                output.write("\n\n"+line+"\n")
            else:
                output.write(line)
        output.write("\n")


convert_pdf_to_txt("data/original/09 PAINTER.pdf")

