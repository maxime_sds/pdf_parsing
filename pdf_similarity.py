# Program to measure the similarity between
# two sentences using cosine similarity.
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import json

def similarities(path1, path2,path3, threshold):
    X=[]
    Y=[]
    Z=[]
    temp="1"
    sections1=[]
    sections2=[]
    sections3=[]
    f1 = open(path1, "r")
    for x in f1:
        if "--SECTION-- " in x:
            X.append(temp)
            temp = x
            sections1.append(x)
        else:
            temp += x
    f2 = open(path2, "r")
    temp="1"
    for y in f2:
        if "--SECTION-- " in y:
            Y.append(temp)
            temp = y
            sections2.append(y)
        else:
            temp += y
    f3 = open(path3, "r")
    temp = "1"
    for z in f3:
        if "--SECTION-- " in z:
            Z.append(temp)
            temp = z
            sections3.append(z)
        else:
            temp += z

    # tokenization
    k=0
    fsi = open("similarities.json", "w")
    fsi.write("similarities between "+path1+" and "+path2+"\n")
    fsi.write("\n")
    for x in X:
        j=0
        cosine = []
        for y in Y:
            X_list = word_tokenize(x)
            Y_list = word_tokenize(y)

            # sw contains the list of stopwords
            sw = stopwords.words('english')
            l1 =[];l2 =[]

            # remove stop words from the string
            X_set = {w for w in X_list if not w in sw}
            Y_set = {w for w in Y_list if not w in sw}

            # form a set containing keywords of both strings
            rvector = X_set.union(Y_set)
            for w in rvector:
                if w in X_set: l1.append(1) # create a vector
                else: l1.append(0)
                if w in Y_set: l2.append(1)
                else: l2.append(0)
            c = 0

            # cosine formula
            for i in range(len(rvector)):
                    c+= l1[i]*l2[i]
            si = c / float((sum(l1) * sum(l2)) ** 0.5)
            if si >= threshold:
                cosine.append([si, sections2[j] ])
            j += 1
        cosine.sort(reverse=True)
        fsi.write(json.dumps({'section': sections1[k] , 'similarities': cosine}))
        fsi.write("\n")
        k+= 1
    k=0
    fsi.write("\n")
    fsi.write("similarities between " + path1 + " and " + path3 + "\n")
    fsi.write("\n")
    for x in X:
        j = 0
        cosine = []
        for z in Z:
            X_list = word_tokenize(x)
            Z_list = word_tokenize(z)

            # sw contains the list of stopwords
            sw = stopwords.words('english')
            l1 = []
            l2 = []

            # remove stop words from the string
            X_set = {w for w in X_list if not w in sw}
            Z_set = {w for w in Z_list if not w in sw}

            # form a set containing keywords of both strings
            rvector = X_set.union(Z_set)
            for w in rvector:
                if w in X_set:
                    l1.append(1)  # create a vector
                else:
                    l1.append(0)
                if w in Z_set:
                    l2.append(1)
                else:
                    l2.append(0)
            c = 0

            # cosine formula
            for i in range(len(rvector)):
                c += l1[i] * l2[i]
            si=c / float((sum(l1) * sum(l2)) ** 0.5)
            if si>=threshold:
                cosine.append([si, sections2[j]])
            j += 1
        cosine.sort(reverse=True)
        fsi.write(json.dumps({'section': sections1[k], 'similarities': cosine}))
        fsi.write("\n")
        k += 1
similarities("out06.txt","out07.txt","out09.txt",0.5)